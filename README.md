# AB Scripts

## Put minified version in zookeeper under abParams

Sample Json structure

```json
{
  "abParams": {
    "conciseView": {
      "params": {
        "minUserId": 100,
        "mod": 3,
        "remainder": [0, 1],
        "defaultPath": false
      },
      "func": "(function(){})"
    }
  }
}
```
