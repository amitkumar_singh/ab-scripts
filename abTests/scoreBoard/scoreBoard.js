var abParams = {
  "params": {
    "minUserId": 1047407,
    "mod": 2,
    "remainder": [1],
    "defaultPath": false
  },
  func: function scoreBoard(abParams, userParams, hmsData) {
    try {
      if (userParams && userParams.userId) {
        const userId = userParams.userId;

        if (userId < abParams.minUserId) {
          return true;
        }

        const remainder = userId % abParams.mod;
        if (abParams.remainder.indexOf(remainder) > -1) {
          return true;
        }
      }
    } catch (err) {
      console.log(err);
    }
    return false;
  }
}
exports.script=abParams.func
