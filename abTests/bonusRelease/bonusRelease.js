
var abParams = {
  "params": {
    "minUserId": 0,
    "mod": 2,
    "remainder": [0, 1]
  },func:function bonusRelease(abParams, userParams, hmsData) {
    try {
      hmsData = hmsData || {};

      const userId = userParams.userId;

      const remainder = userId % abParams.mod;
      const modParams = abParams.modParams;

      /**
       * If abParams has key modParams; compare min and max value for remainder
       */
      if(modParams
        && typeof(modParams.min) === "number"
        && typeof(modParams.max) === "number") {
          return (remainder <= modParams.max && remainder >= modParams.min);
      }
      return (abParams.remainder.indexOf(remainder) > -1);
    } catch (err) {
      console.log(err);
    }
    return false;
  }
}

exports.script = abParams.func;
