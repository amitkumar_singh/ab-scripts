var abParams = {
  "params": {
    "minUserId": 0,
    "mod": 1,
    "remainder": [2],
    "defaultPath": false
  },func:function showCV(abParams, userParams, hmsData, appFlavor) {
    try {
      hmsData = hmsData || {};
  
      const userId = userParams.userId;
      const hmsAttributes = (hmsData.attributes || {});
  
      function showAsPerAB() {
        const remainder = userId % abParams.mod;
        return (abParams.remainder.indexOf(remainder) > -1);
      }
  
      const hasJoinedAnyContest = hmsAttributes.hasJoinedAnyContest;
  
  
      // Case 1: If the user has already joined any contest. Do not show CV.
      if (hasJoinedAnyContest !== false) {
        return false; // do not show concise view
      }
  
      // Case 2: New users
      // New user who for sure will have new build.
      // The only exception here is people who have old builds
      // and they have not signed-up yet or they have got old build from their
      // friend via watsapp, chat etc.
      if (userId > abParams.minUserId) {
        return showAsPerAB();
      }
  
  
      // Case 3: Old user who are switching for the first time.
      // - oldUserEligibleForCV variable will be set first time user switches
      // - (hmsAttributes.appToggleCount <= 1 && 'RCPrimary App') clause
      // is just added to handle the first time switch case
      // because right now oldUserEligibleForCV is not yet available
      // it will be available from next time onwards
      if (hmsAttributes.oldUserEligibleForCV
      || (hmsAttributes.appToggleCount <= 1 && appFlavor === 'rc_primary')) {
        return showAsPerAB();
      }
    } catch (error) {
    }
  
    return false;
  }
}

exports.script=abParams.func